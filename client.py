import sys
import time
from typing import List, Optional

version = "0.1.0"
col2str = "abcdef"
dir2str = "↑↗→↘↓↙←↖"
dir2dxdy = [(0, -1), (1, -1), (1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1)]

max_mem = None
num_cores = None
player = None
board = [None] * 5


def send(msg):
    print(msg, flush=True)


def log(msg):
    print(msg, flush=True, file=sys.stderr)


def pos2str(row_idx: int, col_idx: int) -> str:
    return col2str[col_idx] + str(5 - row_idx)


def reset_game_state():
    board[0] = list("BBBBBC")
    board[1] = list("EEEEEC")
    board[2] = list("EENEEC")
    board[3] = list("EEEEEC")
    board[4] = list("WWWWWC")
    global player
    player = None


def set_board_from_nsn(nsn: List[str]):
    rows = nsn[0].split("/")
    for i, row in enumerate(rows):
        board[i] = list(row)
    if nsn[1] != f"{player}N":
        log(f"Unexpected game stage '{nsn[1]}', expected '{player}N'")


def is_free(col: int, row: int) -> bool:
    return 0 <= col < 6 and 0 <= row < 5 and board[row][col] == "E"


def dry_move(col_idx: int, row_idx: int, dir: int) -> (int, int):
    curr_col, curr_row = col_idx, row_idx
    dx, dy = dir2dxdy[dir]
    next_col, next_row = curr_col + dx, curr_row + dy
    while is_free(next_col, next_row):
        curr_col, curr_row = next_col, next_row
        dx, dy = dir2dxdy[dir]
        next_col, next_row = curr_col + dx, curr_row + dy
    return curr_col, curr_row


def opponent(player: str):
    return "W" if player == "B" else "B"


def winner(current_player: str) -> Optional[str]:
    if "N" in board[0]:
        return "W"
    if "N" in board[4]:
        return "B"

    for n_row in range(1, 4):
        if "N" not in board[n_row]:
            continue
        n_col = board[n_row].index("N")
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                neigh_col = n_col + dx
                neigh_row = n_row + dy
                if is_free(neigh_col, neigh_row):
                    return None
    return opponent(current_player)


def find_turn() -> str:
    """Just choose the first valid move we find"""
    # Move neutrino
    for i, row in enumerate(board):
        if "N" not in row:
            continue
        j = row.index("N")
        for dir in range(8):
            new_col, new_row = dry_move(j, i, dir)
            if new_row != i or new_col != j:
                n_row = new_row
                n_col = new_col
                board[i][j] = "E"
                board[n_row][n_col] = "N"
                break
        break

    conc = winner(player)
    if conc is not None:
        return f"{pos2str(n_row, n_col)} {conc}"

    # Move an own-piece
    found_o = False
    for i, row in enumerate(board):
        for j, piece in enumerate(row):
            if piece != player:
                continue
            for dir in range(8):
                new_col, new_row = dry_move(j, i, dir)
                if new_row != i or new_col != j:
                    o_row = new_row
                    o_col = new_col
                    o_dir = dir
                    board[i][j] = "E"
                    board[o_row][o_col] = player
                    found_o = True
                    break
            if found_o:
                break
        if found_o:
            break

    actions = f"{pos2str(n_row, n_col)} {dir2str[o_dir]}{pos2str(o_row, o_col)}"
    conc = winner(opponent(player))
    if conc is None:
        return actions
    else:
        return f"{actions} {conc}"


def setup_stage():
    protocol_name, version = input().split()
    if protocol_name.lower() != "uni":
        raise SystemExit(f"Did not understand protocol name: {protocol_name}")
    major, minor, patch = map(int, version.split("."))
    if major != 0 or minor != 1:
        raise SystemExit(f"Can only handle version 0.1.x, not: {version}")
    _, charset = input().split()
    if charset.lower() != "utf-8":
        raise SystemExit(f"Can only handle charset utf-8, not: {charset}")
    _, max_mem_ = input().split()
    max_mem_ = int(max_mem_)
    if max_mem_ < 16 * 1024**2:
        raise SystemExit(f"Memory allowance is too low: {max_mem_}")
    global max_mem
    max_mem = max_mem_
    _, num_cores_ = input().split()
    num_cores_ = int(num_cores_)
    if num_cores_ < 1:
        raise SystemExit(f"Engine needs at least 1 CPU core, not: {num_cores_}")
    global num_cores
    num_cores = num_cores_

    line = input()
    if line != "uni_hello_end":
        raise SystemExit(f"Expected uni_hello_end, got: {line}")

    send(f"id name Simpletonian v{version}")
    send("capability nan_version 1.1.0")
    send("capability nsn_version 0.1.1")
    send("capability game_state nsn")
    # We are (hopefully) pretty fast so let's just say we support time
    # control
    send("capability time_control true")
    send("capability tc_increment true")
    send("capability tc_delay true")
    send("capability tc_hourglass true")
    send("capability tc_max_per_turn true")
    send("capability stop_go false")
    send("uni_ok")


def idle_stage() -> bool:
    line = input().split(maxsplit=1)
    if line[0] == "quit":
        return False
    elif line[0] == "new_game":
        if line[1] != "neutrino_standard":
            raise SystemExit(f"Unknown game mode: {line[1]}")
        reset_game_state()
        while True:
            line = input().split()
            if line[0] == "player":
                global player
                player = line[1]
            elif line[0] == "new_game_parameters_end":
                break
            else:
                # Just ignore all the other parameters, they will not
                # affect our simple strategy
                continue
        send("ready_for_new_game")
        return True
    else:
        raise SystemExit(f"Unknown command in idle stage: {line[0]}")


def play_stage() -> bool:
    while True:
        line = input().split()
        if line[0] == "stop_game":
            send("game_stopped")
            return True
        elif line[0] == "game_state":
            if line[1] != "nsn":
                raise SystemExit(f"Unexpected game state format, expected: 'nsn', got: '{line[1]}'")
            set_board_from_nsn(line[2:])
            send("ready_for_go")
        else:
            raise SystemExit(f"Unexpected command in early play stage: {line[0]}")

        line = input().split()
        if line[0] == "stop_game":
            send("game_stopped")
            return True
        elif line[0] == "go":
            t0 = time.perf_counter()
            # Ignore any time control info
            turn = find_turn()
            # We are unstoppable (due to our capabilities), so no need
            # to listen for `stop_go`
            send(f"best_turn {turn}")
            t1 = time.perf_counter()
            log(f"Time taken: {(t1 - t0) * 1000:.2f} ms")
        else:
            raise SystemExit(f"Unexpected command in mid play stage: {line[0]}")


def main():
    setup_stage()

    while True:
        should_continue = idle_stage()
        if not should_continue:
            break
        should_continue = play_stage()
        if not should_continue:
            break


if __name__ == "__main__":
    main()
